package com.co.testingfundamentals

data class User(var name: String = "",
                var age: Int = 0,
                var isHuman: Boolean = true)
