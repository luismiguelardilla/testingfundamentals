package com.co.testingfundamentals

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import kotlin.random.Random

class AssertionsTest {

    @Before

    @Test
    fun getArrayTest() {
        val assertions = Assertions()
        val array = arrayOf(21, 117)
        assertArrayEquals("error en testing", array, assertions.getLuckyNumbers())
    }

    @Test
    fun getNameTest() {
        val assertions = Assertions()
        val name = "Alain"
        val otherName = "Max"
        assertEquals(name, assertions.getName())
        assertNotEquals(otherName, assertions.getName())
    }

    @Test
    fun checkHumanTest() {
        val assertions = Assertions()
        val bot = User("8bit", 1, false)
        val juan = User("Juan ", 18, true)
        assertFalse(assertions.checkHuman(bot))
        assertTrue(assertions.checkHuman(juan))
    }

    @Test
    fun checkNullUserTest() {
        val assertions = Assertions()
        val user = null
        assertNull(user)
        assertNull(assertions.checkHuman(user))
    }

    @Test
    fun checkNotNullUserTest() {
        val assertions = Assertions()
        val juan = User("Juan ", 18, true)
        assertNotNull(juan)
        assertNotNull(assertions.checkHuman(juan))
    }

    @Test
    fun checkNotSameUserTest() {
        val bot = User("8bit", 1, false)
        val juan = User("Juan ", 18, true)
        assertNotSame(bot, juan)
    }

    @Test
    fun checkSameUserTest() {
        val bot = User("Juan ", 18, true)
        val juan = User("Juan ", 18, true)
        val copyJuan = juan
        assertSame(copyJuan, juan)
        assertEquals(bot, juan)
    }

    @Test(timeout = 1000)
    fun getCitiesTest() {
        val cities = arrayOf("México", "Perú", "Argentina")
        //Thread.sleep(Random.nextLong(200, 1100))
        Thread.sleep(Random.nextLong(950, 1050))
        assertEquals(3, cities.size)
    }


}